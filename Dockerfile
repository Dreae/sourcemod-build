FROM fedora:27
RUN dnf -y update
RUN dnf -y install python-beautifulsoup4 libstdc++.i686
RUN pip install requests
WORKDIR /sourcemod

COPY get_stable_url get_stable_url
RUN curl -o sm.tar.gz `./get_stable_url`

RUN tar -xvf sm.tar.gz
RUN cp -R /sourcemod/addons/sourcemod/scripting .

ENTRYPOINT ["/sourcemod/scripting/spcomp"]
